- name: Custom Wordle
  url: https://mywordle.me/
  twist: Make your own wordle
  emoji: ✍
  media:
    - url: https://nte.unifr.ch/blog/2022/01/24/un-peu-de-detente-wordle-en-francais-und-auf-deutsch/
    - url: https://www.microsiervos.com/archivo/juegos-y-diversion/custom-wordle-crear-wordles-palabras-longitud-idioma.html

- name: word.rodeo
  url: https://word.rodeo/
  twist: 'Make your own wordle. Supports: Deutsch, English, español, français, Nederlands, norsk, polski.'
  emoji: ✍

- name: Wordle Together
  url: https://wordletogether.com/
  twist: Play against a friend
  emoji: 🤝
  media:
    - url: https://www.microsiervos.com/archivo/puzzles-y-rubik/variantes-wordle.html

- name: Wordle Online
  url: https://wordler.netlify.app/
  twist: Play against a friend or a random opponent
  emoji: 🤝

- name: Word Race
  url: https://metzger.media/games/word-race/
  twist: Play against a random opponent
  emoji: 🏎

- name: Weredle
  url: https://weredle.netlify.app/
  src: https://github.com/justindwyer6/weredle
  src-type: github
  twist: Wordle with a Werewolf twist
  emoji: 🐺

- name: Pfeffel
  url: https://www.conniptions.org/pfeffel/
  src: https://github.com/wgmyers/pfeffel/
  src-type: github
  twist: '<a href="https://en.wikipedia.org/wiki/Liar_paradox">Always lies</a> about letter placements'
  emoji: 🤥
  developer:
    name: Wayne Myers
    url: https://www.yutani.org/
  announcement:
    date: 2022-01-12
    url: https://twitter.com/b3ta_links/status/1481351551801303047
  reference: https://twitter.com/conniptions/status/1489950865976922117

- name: Eldrow
  url: https://eldrow.io/
  twist: Find words matching a particular coloured solution
  emoji: 🟨

- name: Crosswordle
  url: https://crosswordle.vercel.app/
  twist: Find words matching a particular coloured solution
  emoji: 🟨

- name: Dordle
  url: https://zaratustra.itch.io/dordle
  twist: Solve two words at once; daily or unlimited
  emoji: ✌

- name: Quordle
  url: https://www.quordle.com/
  src: https://github.com/fireph/quordle/
  src-type: github
  twist: Solve four words at once; daily or unlimited
  emoji: 🍀
  developer:
    name: Freddie Meyer

- name: seven wordles
  url: https://www.wooferzfg.me/seven-wordles/
  src: https://github.com/wooferzfg/seven-wordles/
  src-type: github
  twist: Guess seven words as quickly as possible
  emoji: 7️⃣

- name: Obscurdle
  url: https://obscurdle.com/
  twist: Find words fitting requested colour patterns
  emoji: 🧩

- name: Pictle
  url: https://pictle.web.app/
  twist: Find words fitting an emoji grid and a target word
  emoji: 🖼
  developer:
    name: Michael Bleigh
    url: https://twitter.com/mbleigh
  announcement:
    date: 2022-01-19
    url: https://twitter.com/mbleigh/status/1483921835620139009

- name: Heardle
  url: https://heardle.glitch.me/
  src: https://glitch.com/edit/#!/heardle
  src-type: glitch
  twist: Phonemic version (US English)
  emoji: 🗣

- name: wɚdəl
  url: https://manishearth.github.io/ipadle/
  src: https://github.com/manishearth/ipadle/
  src-type: github
  twist: Phonemic version (US English)
  emoji: 🗣

- name: WORD/*L
  url: http://word-l.herokuapp.com/
  twist: Stenotype outlines
  emoji: 🎹

- name: Absurdle
  url: https://qntm.org/files/wordle/
  twist: Adversarial variant
  emoji: 😈
  media:
    - url: https://stackdiary.com/wordle-alternatives/
    - url: https://www.microsiervos.com/archivo/juegos-y-diversion/absurdle-wordle-cabron-dificil.html

- name: Evil Wordle
  url: https://swag.github.io/evil-wordle/
  src: https://github.com/swag/swag.github.io/tree/master/evil-wordle
  src-type: github
  twist: Similar to Absurdle
  emoji: 😈
  media:
    - url: https://stackdiary.com/wordle-alternatives/

- name: '<span aria-label="Eldrow">Wordle</span>'
  css: mirrored
  url: https://www.simn.me/eldrow
  src: https://github.com/xsznix/eldrow
  src-type: github
  twist: "Switch places: the computer makes guesses and you give feedback"
  emoji: 🔄
  developer:
    name: Xuming Zeng
    url: https://www.simn.me/
  announcement:
    date: 2022-01-26
    url: https://twitter.com/xsznix/status/1486457358494666755

- name: Squardle
  url: https://fubargames.se/squardle/
  twist: 'Solve 5×5 <a href="https://en.wikipedia.org/wiki/Magic_square">magic square</a> with four holes'
  emoji: 🔲

- name: Vardle
  url: https://vardle.netlify.app/
  twist: Length of target varies (2–11 letters); coloured hints help with discovering this new variable
  emoji: ❓

- name: Wheredle
  url: https://calvinballing.github.io/wheredle/
  src: https://github.com/calvinballing/wheredle/
  src-type: github
  twist: The five-letter word is located somewhere within a ten-tile space; seven attempts
  emoji: 🟦
  developer:
    name: Jim Hays
    url: https://calvinballing.github.io/
  announcement:
    date: 2022-02-02
    url: https://twitter.com/calvinballing/status/1488766327770816513

- name: Weird Wordle
  url: https://weird-wordles.netlify.app/
  twist: 'The gameplay twist changes every week'
  emoji: 🤔

- name: Wordle by the hour
  url: https://binaryinitials.com/wordle/
  twist: New word each hour
  emoji: 🕐

- name: hello wordl
  url: https://hellowordl.net/
  src: https://github.com/lynn/hello-wordl/
  src-type: github
  twist: Choose target length (4–11 letters); more than one word per day
  emoji: ♾
  media:
    - url: https://stackdiary.com/wordle-alternatives/

- name: Wordle Unlimited
  url: https://www.wordleunlimited.com/
  twist: Choose target length (4–11 letters); more than one word per day
  emoji: ♾

- name: Wordle Game
  url: https://wordle-play.com/
  twist: Choose target length (5–7 letters); more than one word per day; fraudulent
  emoji: ♾

- name: Wordle-Unlimited
  url: https://wordle-unlimited.glitch.me/
  src: https://glitch.com/edit/#!/wordle-unlimited
  src-type: glitch
  twist: Refresh to get a new word; fraudulent
  emoji: ♾

- name: Wordle+
  url: https://mikhad.github.io/wordle/
  src: https://github.com/mikhad/wordle/
  src-type: github
  twist: New word each day / hour / unlimited
  emoji: ♾

- name: Plurdle
  url: https://woolly.one/plurdle/
  twist: Play multiple words a day
  emoji: ♾

- name: Web Wordle
  url: https://www.webwordle.com/
  twist: Play multiple words a day; fraudulent
  emoji: ♾
  media:
    - url: https://stackdiary.com/wordle-alternatives/

- name: 6by6
  url: https://www.danielalbu.com/6by6/
  twist: Six letter words
  emoji: 6️⃣
  developer:
    name: Daniel Albu
    url: https://www.danielalbu.com/

- name: "Wordle 2"
  url: https://www.wordle2.in/
  twist: Six letter words; fraudulent
  emoji: 6️⃣

- name: Guessle
  url: https://guessle.herokuapp.com/
  src: https://github.com/jakerella/guessle
  src-type: github
  twist: 'Customisable: 5–6 letters, three levels of lexical ‘difficulty’, in-/excluding words with repeating letters'
  emoji: ⚙

- name: Luckle
  url: https://leahneukirchen.github.io/luckle/
  src: https://github.com/leahneukirchen/luckle
  src-type: github
  twist: For people who are lucky
  emoji: 🍀
  developer:
    name: Leah Neukirchen
    url: https://leahneukirchen.org/
  announcement:
    date: 2022-01-18
    url: https://twitter.com/LeahNeukirchen/status/1483553379544666114

- name: Easy Wordle
  url: https://www.easywordle.com/
  twist: For people who are lucky and want to take it easy
  emoji: 🍀
